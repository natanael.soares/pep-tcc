import React, { Component } from 'react';
import { getTransacao } from '../config/API'
import Pacientes from './Pacientes'
import Medicos from './Medicos'
import Consultas from './Consultas'

class BlockchainList extends Component {
    constructor(props){
        super(props);
        this.state = {
            transacao: {},
            update:true
        }
        this.getApi(); 
    }

    getApi = () => {
        this.setState({update:false});
        var result = getTransacao(this.props.transacao.tipo,
            this.props.transacao.id);
        result.then((result)=>{   
            this.setState({transacao:result});                    
            console.log('BlockchainList: ' +result);
        });               
    }

    SwitchElement = () => {
        switch (this.props.transacao.tipo){
            case 'novoPaciente':
              return <Pacientes paciente={this.state.transacao}/>

            case 'novoMedico':
              return <Medicos medico={this.state.transacao}/>

            case 'novaConsulta':
              return <Consultas consulta={this.state.transacao}/>     
      
          }
    }

    render() {         
        return (
        <div>    
             
            <div>
                {this.SwitchElement()}
            </div>           
        </div>
        );
    }
}

export default BlockchainList;