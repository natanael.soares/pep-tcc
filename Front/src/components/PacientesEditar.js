import React, { Component } from 'react';
import { Input, Modal, Button, message } from 'antd';
import PacientesCadastrar from './PacientesCadastrar';


class PacientesEditar extends Component {
    constructor(props){
        super(props);
        this.state = {visible:false};
    }

    showModal = () => {
        this.setState({
          visible: true,
        });
    }
    

    handleCancel = (e) => {
        console.log(e);
        this.setState({
          visible: false,
        });          
    }

    handleOk = (e) => {
        console.log('ok!!!');
    }

    
    render() {  
        console.log('PacientesEditar: '+this.props.paciente.nome); 
        return (             
            <div>
                <Button type="primary" ghost onClick={this.showModal}>Editar</Button>
                <Modal
                    title="Editar paciente"
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}>

                    <div>
                        <PacientesCadastrar paciente={this.props.paciente} operacao='Editar' />
                    </div>         
                </Modal>
               
            </div>            
        );
    }
}

export default PacientesEditar;
