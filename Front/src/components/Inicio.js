import React, { Component } from 'react';

class Inicio extends Component {
  render() {
    return (
      <div>
        Trabalho de conclusão de curso de Natanael R. Soares <br/>

        Prontuário Eletrônico do Paciente, uma implementação utilizando blockchain. <br/><br/>     
        Front End para validação da aplicação, onde é possível a inserção de supervisores, medicos e pacientes.<br/>
        Como apresentado na monografia, cada participante possue suas permissões, aqui será visto em pratica o permissionamento.<br/>
        Para utilização deste site, é necessaria a autenticação e a inserção de uma identidade na carteira do usuário. <br/>
        Por fim, o sistema de token é utilizado.
      </div>
    );
  }
}

export default Inicio;
