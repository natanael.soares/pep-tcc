import React, { Component } from 'react';
import { Input, Table, Divider } from 'antd';
import { getPacientes } from '../config/API'
import PacientesEditar from './PacientesEditar';
import PacientesRemover from './PacientesRemover'

const Search = Input.Search;

const columns = [{
          title: 'Cpf',
          dataIndex: 'cpf',
          key: 'cpf',
          render: text => <a href="javascript:;">{text}</a>,
        }, {
          title: 'Nome',
          dataIndex: 'nome',
          key: 'nome',
        }, {
          title: 'Endereco',
          dataIndex: 'endereco',
          key: 'endereco',
        }, {
            title: 'Nascimento',
            dataIndex: 'dataNascimento',
            key: 'dataNascimento',
        }/*, {
            title: 'Mais',
            key: 'mais',
            render: (text, record) => (
              <span>
                <PacientesEditar paciente={record}/>
                <PacientesRemover paciente={record}/>
              </span>
            ),
          }*/];

class PacientesListar extends Component {
    constructor(props){
        super(props);
        this.state = {pacientes: []};
    }

    onSearch = (cpf) => {
        var result = getPacientes(cpf);
        result.then((result)=>{                       
            this.setState({pacientes:result})
        });
    }
    
    render() {  
        return (             
            <div>
                <Search
                  placeholder="CPF Paciente"
                  onSearch={this.onSearch}
                  style={{ width: 200 }}
                /> 
                <Table columns={columns} dataSource={this.state.pacientes} />               
            </div> 
        );
    }
}

export default PacientesListar;
