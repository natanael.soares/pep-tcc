import React, { Component } from 'react';
import { Form, Input, Button, message } from 'antd';
import { CadastraMedico } from '../config/API'

const FormItem = Form.Item;

class MedicosCadastrar extends Component {
  constructor(props){
    super(props);
    this.state = { cpf: '',
                  nome: '',
                  endereco: '',
                  especificacao: '',
                  registro: '',
                  dataNascimento: ''};
  }
  
  submit = () => {
    if(this.state.cpf.length < 3 ||
      this.state.nome.length < 3 ||
      this.state.endereco.length < 3 ||
      this.state.especificacao.length < 3 ||
      this.state.registro.length < 3 ||
      this.state.dataNascimento.length < 3){
        message.warning('Campos invalidos!');
      }
    else{
      var resposta = CadastraMedico(this.state.cpf, this.state.nome, this.state.registro, this.state.especificacao,
          this.state.endereco, this.state.dataNascimento);
      resposta.then((result)=>{
        if(result)
          message.success('Medico cadastrado com sucesso!');        
        else
          message.error('Erro ao cadastrar o médico!');        
      });      
    }
  }

  changeCpf = (e) => {
    this.setState({cpf:e.target.value});
  }
  changeNome = (e) => {
    this.setState({nome:e.target.value});
  }
  changeEndereco = (e) => {
    this.setState({endereco:e.target.value});
  }
  changeEspecificacao = (e) => {
    this.setState({especificacao:e.target.value});
  }
  changeRegistro = (e) => {
    this.setState({registro:e.target.value});
  }
  changeDataNascimento = (e) => {
    this.setState({dataNascimento:e.target.value});
  }

  render() {
    return (
      <div>
        <Form layout='horizontal'>
            <FormItem
              label="Cpf">
              <Input onChange={this.changeCpf} placeholder="Insira o cpf" />
            </FormItem>
            <FormItem
              label="Nome">
              <Input onChange={this.changeNome} placeholder="Insira o nome" />
            </FormItem>
            <FormItem
              label="Registro">
              <Input onChange={this.changeRegistro} placeholder="Insira o registro" />
            </FormItem>
            <FormItem
              label="Especificação">
              <Input onChange={this.changeEspecificacao} placeholder="Ortopedista ou Ginecologista" />
            </FormItem>
            <FormItem
              label="Endereco">
              <Input onChange={this.changeEndereco} placeholder="Insira o endereço" />
            </FormItem>
            <FormItem
              label="Data Nascimento">
              <Input onChange={this.changeDataNascimento} placeholder="Insira a Data de Nascimento" />
            </FormItem>
            <FormItem>
              <Button onClick={this.submit} type="primary">Cadastrar</Button>
            </FormItem>
        </Form>
      </div>
    );
  }
}

export default MedicosCadastrar;
 