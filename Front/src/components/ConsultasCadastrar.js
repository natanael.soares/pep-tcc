import React, { Component } from 'react';
import { Form, Input, Button, message } from 'antd';
import { CadastraConsulta } from '../config/API'

const FormItem = Form.Item;
const { TextArea } = Input;

class ConsultasCadastrar extends Component {
  constructor(props){
    super(props);
    this.state = {paciente: '',
                  medico: '',
                  anamnese: '',
                  historico: '',
                  exameFisico: '',
                  examesSolicitados: '',
                  hipotese: '',
                  conduta: '',
                  exames: ''};
  }
  
  submit = () => {
    if(this.state.paciente.length < 3 ||
      this.state.medico.length < 3){
        message.warning('Campos invalidos!');
      }
    else{
      var exames = new Array();
      exames.push(this.state.exames);
      exames.push(this.state.exames);
      var resposta = CadastraConsulta(this.state.paciente, this.state.medico, this.state.anamnese, this.state.historico,
          this.state.exameFisico, this.state.examesSolicitados, this.state.hipotese, this.state.conduta, exames);
      resposta.then((result)=>{
        if(result)
          message.success('Consulta cadastrada com sucesso!');        
        else
          message.error('Erro ao cadastrar consulta!');        
      });      
    }
  }

  changePaciente = (e) => {
    this.setState({paciente:e.target.value});
  }
  changeMedico = (e) => {
    this.setState({medico:e.target.value});
  }
  changeAnamnese = (e) => {
    this.setState({anamnese:e.target.value});
  }
  changeHistorico = (e) => {
    this.setState({historico:e.target.value});
  }
  changeExameFisico = (e) => {
    this.setState({exameFisico:e.target.value});
  }
  changeExamesSolicitados = (e) => {
    this.setState({examesSolicitados:e.target.value});
  }
  changeHipotese = (e) => {
    this.setState({hipotese:e.target.value});
  }
  changeConduta = (e) => {
    this.setState({conduta:e.target.value});
  }
  changeExames = (e) => {
    this.setState({exames:e.target.value});
  }


  render() {
    return (
      <div>
        <Form layout='horizontal'>
            <FormItem
              label="Paciente">
              <Input onChange={this.changePaciente} placeholder="Insira o cpf do paciente" />
            </FormItem>
            <FormItem
              label="Medico">
              <Input onChange={this.changeMedico} placeholder="Insira o cpf do medico" />
            </FormItem>
            <FormItem
              label="Anamnese">
              <TextArea onChange={this.changeAnamnese} placeholder="Insira a anamnese" />
            </FormItem>
            <FormItem
              label="Historico">
              <TextArea onChange={this.changeHistorico} placeholder="Insira o historico" />
            </FormItem>
            <FormItem
              label="Exame Fisico">
              <TextArea onChange={this.changeExameFisico} placeholder="Insira o exame físico" />
            </FormItem>
            <FormItem
              label="Exames Solicitados">
              <TextArea onChange={this.changeExamesSolicitados} placeholder="Insira os exames solicitados" />
            </FormItem>
            <FormItem
              label="Hipotese">
              <TextArea onChange={this.changeHipotese} placeholder="Insira a hipotese" />
            </FormItem>
            <FormItem
              label="Conduta">
              <TextArea onChange={this.changeConduta} placeholder="Insira a conduta" />
            </FormItem>
            <FormItem
              label="Exames">
              <TextArea onChange={this.changeExames} placeholder="Insira os exames" />
            </FormItem>
            <FormItem>
              <Button onClick={this.submit} type="primary">Cadastrar</Button>
            </FormItem>
        </Form>
      </div>
    );
  }
}

export default ConsultasCadastrar;
 