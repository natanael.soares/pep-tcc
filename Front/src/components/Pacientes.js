import React, { Component } from 'react';

class Pacientes extends Component {
  render() {
    return (
      <div>
          <ol>
            <li>Cpf: {this.props.paciente.cpf}</li>
            <li>Nome: {this.props.paciente.nome}</li>
            <li>Endereco: {this.props.paciente.endereco}</li>
            <li>Data Nascimento: {this.props.paciente.dataNascimento}</li>
        </ol>    
      </div>
    );
  }
}

export default Pacientes;
