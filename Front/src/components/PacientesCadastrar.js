import React, { Component } from 'react';
import { Form, Input, Button, message } from 'antd';
import { CadastraPaciente } from '../config/API'

const FormItem = Form.Item;

class PacientesCadastrar extends Component {
  constructor(props){
    super(props);
    this.state = { cpf: '',
                  nome: '',
                  endereco: '',
                  dataNascimento: ''};
  }
  
  submit = () => {
    if(this.state.cpf.length < 3 ||
      this.state.nome.length < 3 ||
      this.state.endereco.length < 3 ||
      this.state.dataNascimento.length < 3){
        message.warning('Campos invalidos!');
      }
    else{
      var resposta = CadastraPaciente(this.state.cpf, this.state.nome, this.state.endereco, this.state.dataNascimento);
      resposta.then((result)=>{
        if(result)
          message.success('Paciente cadastrado com sucesso!');        
        else
          message.error('Erro ao cadastrar o paciente!');        
      });
      
    }
  }

  changeCpf = (e) => {
    this.setState({cpf:e.target.value});
  }
  changeNome = (e) => {
    this.setState({nome:e.target.value});
  }
  changeEndereco = (e) => {
    this.setState({endereco:e.target.value});
  }
  changeDataNascimento = (e) => {
    this.setState({dataNascimento:e.target.value});
  }

  render() {
    console.log('PacienteCadastrar: '+this.props.operacao)
    return (
      <div>
        <Form layout='horizontal'>
            <FormItem
              label="Cpf">
              <Input onChange={this.changeCpf} placeholder={this.props.paciente.cpf} />
            </FormItem>
            <FormItem
              label="Nome">
              <Input onChange={this.changeNome} placeholder= {this.props.paciente.nome}/>
            </FormItem>
            <FormItem
              label="Endereco">
              <Input onChange={this.changeEndereco} placeholder={this.props.paciente.endereco} />
            </FormItem>
            <FormItem
              label="Data Nascimento">
              <Input onChange={this.changeDataNascimento} placeholder={this.props.paciente.dataNascimento} />
            </FormItem>
            
            {this.props.operacao == 'Editar' ? null : 
              <FormItem>
                <Button onClick={this.submit} type="primary">{this.props.operacao}</Button>
              </FormItem>
            }
        </Form>
      </div>
    );
  }
}

export default PacientesCadastrar;
 