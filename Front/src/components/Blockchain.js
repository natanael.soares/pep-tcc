import React, { Component } from 'react';
import {  Table } from 'antd';
import { getHistorian } from '../config/API'
import BlockchainList from './BlockchainList'

const columns = [{
          title: 'Id',
          dataIndex: 'id',
          key: 'id',
          render: text => <a href="javascript:;">{text}</a>,
        }, {
          title: 'Tipo',
          dataIndex: 'tipo',
          key: 'tipo',
        }, {
          title: 'Paticipante',
          dataIndex: 'paticipante',
          key: 'paticipante',
        }, {
            title: 'Data',
            dataIndex: 'data',
            key: 'data',
        }];

class Blockchain extends Component {
    constructor(props){
        super(props);
        this.state = {transacoes: []};
        this.update();
    }

    update = () => {
        var result = getHistorian();
        result.then((result)=>{                       
            this.setState({transacoes:result})
        });
    }
    
    render() {  
        
        return (             
            <div>             
              <Table columns={columns} 
                expandedRowRender={ (record,i) => <BlockchainList transacao={this.state.transacoes[i]}/> }
                dataSource={this.state.transacoes} />               
            </div> 
        );
    }
}

export default Blockchain;
