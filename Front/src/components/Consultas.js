import React, { Component } from 'react';

class Consultas extends Component {

  getExames = () => {
    console.log(this.props.consulta.exames);
    if(typeof this.props.consulta.exames != 'undefined')
      return this.props.consulta.exames.map((x,i) => <p>{x}</p>);
    
  }


  render() {
    console.log('Consultas:' +this.props.consulta.exames);
    return (
      <div>     
        <ol>
            <li>Historico: {this.props.consulta.historico}</li>
            <li>Exame Fisico: {this.props.consulta.exameFisico}</li>
            <li>Exames Solicitados: {this.props.consulta.examesSolicitados}</li>
            <li>Hipotese: {this.props.consulta.hipotese}</li>
            <li>Conduta: {this.props.consulta.conduta}</li>
            <li>Exames: {this.getExames()}</li>
        </ol>      
      </div>
    );
  }
}

export default Consultas;