import React, { Component } from 'react';
import NavBar from './NavBar'

import { Layout, Menu } from 'antd';
import Inicio from './Inicio';
import Blockchain from './Blockchain';
import PacientesCadastrar from './PacientesCadastrar'
import MedicosCadastrar from './MedicosCadastrar'
import ConsultasCadastrar from './ConsultasCadastrar'
import PacientesListar from './PacientesListar'
import MedicosListar from './MedicosListar'
import ConsultasListar from './ConsultasListar'
import Info from './Info'

const { Header, Content, Footer, Sider } = Layout;

const cadastrar = {
  cpf: 'Insira o cpf',
  nome: 'Insira o nome',
  endereco: 'Insira o endereco',
  dataNascimento: 'Insira a data nascimento'

}

class Main extends Component {
  constructor(props){
    super(props);
    this.state = {
        current: '',
    }
  }

  handleClick = (e) => {        
      this.setState({
          current: e.key,
      });
  }

  SwitchElement = () => {
    if(this.props.page == 'Inicio')
      return <Inicio />
    if(this.props.page == 'Blockchain')
      return <Blockchain />

    var rota = this.props.page + this.state.current;
    console.log(rota);
    switch (rota){
      case 'PacientesCadastrar':
        return <PacientesCadastrar operacao='Cadastrar' paciente={cadastrar} />

      case 'PacientesListar':
        return <PacientesListar />

      case 'MedicosCadastrar':
        return <MedicosCadastrar />

      case 'MedicosListar':
        return <MedicosListar />

      case 'ConsultasCadastrar':
        return  <ConsultasCadastrar />

      case 'ConsultasListar':
        return  <ConsultasListar />

      default:
        return <Info />
    }
       
  }

  clearAll = () => {
    this.setState({
      current: '',
    });

  }
  
  render() {
    return (
      <Layout>
        <Header className="header"> 
          <div className="logo" />
          <NavBar changePage={this.props.changePage}/>
        </Header>
      <Layout>
        {this.props.page == 'Inicio' ||
          this.props.page == 'Blockchain' ? null :  
          <Sider width={200} style={{ background: '#fff' }}>
            <Menu
              mode="inline"
              onClick={this.handleClick}
              defaultSelectedKeys={['0']}
              defaultOpenKeys={[this.state.current]}
              style={{ height: '100%', borderRight: 0 }}>
              <Menu.Item key="Cadastrar">
                <span className="nav-text">Cadastrar</span>
              </Menu.Item>
              <Menu.Item key="Listar">
                <span className="nav-text">Listar</span>
              </Menu.Item>
            </Menu>
          </Sider>  
        }          
        <Layout style={{ padding: '0 24px 24px' }}>          
          <Content style={{ background: '#fff', padding: 24, margin: 0, minHeight: 550 }}>
            {this.SwitchElement()}
          </Content>
          <Footer style={{ textAlign: 'center' }}>
            Pep - Blockchain 2018 <br/> 
            Natanael R. Soares
          </Footer>
        </Layout>
      </Layout>
    </Layout>
    );
  }
}

export default Main;
