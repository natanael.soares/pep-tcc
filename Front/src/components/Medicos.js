import React, { Component } from 'react';

class Medicos extends Component {
  render() {
    return (
      <div>
          <ol>
            <li>Cpf: {this.props.medico.cpf}</li>
            <li>Nome: {this.props.medico.nome}</li>
            <li>Registro: {this.props.medico.registro}</li>
            <li>Especificação: {this.props.medico.especificacao}</li>
            <li>Endereco: {this.props.medico.endereco}</li>
            <li>Data Nascimento: {this.props.medico.dataNascimento}</li>
        </ol>    
      </div>
    );
  }
}

export default Medicos;
