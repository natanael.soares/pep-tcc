import React, { Component } from 'react';
import { Modal, Button } from 'antd';

const confirm = Modal.confirm;


class PacientesRemover extends Component{    
    
    showConfirm = () => {
        confirm({
            title: 'Você deseja deletar esse paciente?',
            content: 'Paciente: '+this.props.paciente.nome,
            onOk() {
            console.log('Sim');
            },
            onCancel() {
            console.log('Cancelar');
            },
        });
    }

    render(){
        return (
            <div>
                <Button type="primary" onClick={this.showConfirm} ghost>
                    Deletar
                </Button>            
            </div>
        );
    }
    
}

export default PacientesRemover;