import React, { Component } from 'react';
import { Input, Table } from 'antd';
import { getConsultas } from '../config/API'
import Consultas from './Consultas'

const Search = Input.Search;

const columns = [{
          title: 'Tipo',
          dataIndex: 'tipo',
          key: 'tipo',
          render: text => <a href="javascript:;">{text}</a>,
        }, {
          title: 'Medico',
          dataIndex: 'medico',
          key: 'medico',
        }, {
          title: 'Data',
          dataIndex: 'data',
          key: 'data',
        }];

class ConsultasListar extends Component {
    constructor(props){
        super(props);
        this.state = {consultas: []};
    }

    onSearch = (cpf) => {
        var result = getConsultas(cpf);
        result.then((result)=>{                       
            this.setState({consultas:result})
        });
    }
    
    render() {  
        return (             
            <div>
                <Search
                  placeholder="CPF Paciente"
                  onSearch={this.onSearch}
                  style={{ width: 200 }}
                /> 
                <Table columns={columns} 
                    expandedRowRender={ (record,i) => <Consultas consulta={this.state.consultas[i]}/> }
                    dataSource={this.state.consultas} />               
            </div> 
        );
    }
}

export default ConsultasListar;

                        