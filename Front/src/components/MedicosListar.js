import React, { Component } from 'react';
import { Input, Table } from 'antd';
import { getMedicos } from '../config/API'

const Search = Input.Search;

const columns = [{
          title: 'Cpf',
          dataIndex: 'cpf',
          key: 'cpf',
          render: text => <a href="javascript:;">{text}</a>,
        }, {
          title: 'Nome',
          dataIndex: 'nome',
          key: 'nome',
        }, {
            title: 'Registro',
            dataIndex: 'registro',
            key: 'registro',
        }, {
            title: 'Especificacao',
            dataIndex: 'especificacao',
            key: 'especificacao',
        }, {
          title: 'Endereco',
          dataIndex: 'endereco',
          key: 'endereco',
        }, {
            title: 'DataNascimento',
            dataIndex: 'dataNascimento',
            key: 'dataNascimento',
        }];

class MedicosListar extends Component {
    constructor(props){
        super(props);
        this.state = {medicos: []};
    }

    onSearch = (cpf) => {
        var result = getMedicos(cpf);
        result.then((result)=>{                       
            this.setState({medicos:result})
        });
    }
    
    render() {  
        return (             
            <div>
                <Search
                  placeholder="CPF Medico"
                  onSearch={this.onSearch}
                  style={{ width: 200 }}
                /> 
                <Table columns={columns} dataSource={this.state.medicos} />               
            </div> 
        );
    }
}

export default MedicosListar;
