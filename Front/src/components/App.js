import React, { Component } from 'react';
import './App.css';
import { BrowserRouter, Route, Redirect, Switch } from 'react-router-dom';
import Main from './Main'
import Error404 from './Error404'

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      page : 'Inicio'
    }
  }

  changePage = (page) => {
    this.setState({page:page});
    console.log('click ', page);
  }

  render() {
    return (
    <div>
      <BrowserRouter>
            <Switch>
                <Route path='/' render={() => <Main 
                                                  page={this.state.page} 
                                                  changePage={this.changePage}/>} />
                <Route path='/error' render={() => <Error404 />} />
                <Redirect from='*' to='/error' />
            </Switch>
        </BrowserRouter>

    </div>
    );
  }
}

export default App;
