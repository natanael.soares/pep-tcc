import React, { Component } from 'react';
import { Menu } from 'antd';

class NavBar extends Component {
    constructor(props){
        super(props);
        this.state = {
            current: 'Inicio',
        }
    }
    
    handleClick = (e) => {        
        this.setState({
            current: e.key,
        });
        this.props.changePage(e.key);
        //this.props.clearAll();
    }

    render() {
        return (
            <Menu onClick={this.handleClick}
                selectedKeys={[this.state.current]}
                mode="horizontal"
                theme="dark"
                style={{ lineHeight: '64px' }}
                >
                <Menu.Item key="Inicio">
                    Inicio
                </Menu.Item>
                <Menu.Item key="Pacientes">
                    Pacientes
                </Menu.Item>
                <Menu.Item key="Medicos">
                    Medicos
                </Menu.Item>                        
                <Menu.Item key="Consultas">
                    Consultas
                </Menu.Item>
                <Menu.Item key="Blockchain">
                    Blockchain
                </Menu.Item>
            </Menu>
        );
    }
}

export default NavBar;
