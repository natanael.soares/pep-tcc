import request from 'superagent';
import { IP_ADDRES } from './setup'

export const CadastraPaciente = async (cpf, nome, endereco, dataNascimento) => {
    var response = false;
    var rota = IP_ADDRES + 'novoPaciente';
    console.log('POST_Paciente: '+rota);
    var paciente = {cpf:cpf, nome:nome, endereco:endereco, dataNascimento:dataNascimento};
    
    await request.post(rota)
            .send(paciente)
            .set('Accept', 'application/json')
            .set('x-access-token', 'yl8CI1ON7mUDgew5DoiFOCSK8dwYGFU3ENNSvZ4VdXFfzbI2EaLhW1oafyB4sqCq')
            .then(res => {
                console.log('POST_Paciente: '+JSON.stringify(res.body));
                console.log('POST_Paciente: '+res.status);       
                if(res.status==200)
                    response= true;
            })
            .catch(er => {
                console.log('POST_Paciente: error'+er);
            });  
    return response;
}

export const CadastraMedico = async (cpf, nome, registro, especificacao, endereco, dataNascimento) => {
    var response = false;
    var rota = IP_ADDRES + 'novoMedico';
    console.log('POST_Paciente: '+rota);
    var paciente = {cpf:cpf, nome:nome, registro:registro, especificacao:especificacao, 
        endereco:endereco, dataNascimento:dataNascimento};
    
    await request.post(rota)
            .send(paciente)
            .set('Accept', 'application/json')
            .set('x-access-token', 'yl8CI1ON7mUDgew5DoiFOCSK8dwYGFU3ENNSvZ4VdXFfzbI2EaLhW1oafyB4sqCq')
            .then(res => {
                console.log('POST_Paciente: '+JSON.stringify(res.body));
                console.log('POST_Paciente: '+res.status);       
                if(res.status==200)
                    response= true;
            })
            .catch(er => {
                console.log('POST_Paciente: error'+er);
            });  
    return response;
}

export const CadastraConsulta = async (paciente, medico, anamnese, historico, exameFisico, 
    examesSolicitados, hipotese, conduta, exames) => {
    var response = false;
    var rota = IP_ADDRES + 'novaConsulta';
    console.log('POST_Consulta: '+rota);
    var consulta = {paciente:`resource:org.pep.blockchain.paciente.Paciente#${paciente}`, 
                    medico:`resource:org.pep.blockchain.medico.Medico#${medico}`,
                    anamnese:anamnese,
                    historico:historico,
                    exameFisico:exameFisico,
                    examesSolicitados:examesSolicitados,
                    hipotese:hipotese,
                    conduta:conduta,
                    exames:exames    
                };
    
    await request.post(rota)
            .send(consulta)
            .set('Accept', 'application/json')
            .set('x-access-token', 'yl8CI1ON7mUDgew5DoiFOCSK8dwYGFU3ENNSvZ4VdXFfzbI2EaLhW1oafyB4sqCq')
            .then(res => {
                console.log('POST_Consulta: '+JSON.stringify(res.body));
                console.log('POST_Consulta: '+res.status);       
                if(res.status==200)
                    response= true;
            })
            .catch(er => {
                console.log('POST_Consulta: error'+er);
            });  
    return response;
}

export const getPacientes = async (cpf) => {
    var pacientes = new Array();
    var paciente;
    var rota;
    if(cpf.length == 0)
        rota = IP_ADDRES + 'queries/getPacientes';
    else
        rota = IP_ADDRES + `queries/getPaciente?cpf=${cpf}`;
    console.log('GET_Pacientes: '+rota);
    
    await request.get(rota)
            .set('x-access-token', 'yl8CI1ON7mUDgew5DoiFOCSK8dwYGFU3ENNSvZ4VdXFfzbI2EaLhW1oafyB4sqCq')
            .then(res => {
                console.log('GET_Pacientes: '+JSON.stringify(res.body));
                console.log('GET_Pacientes: '+res.status);    
                pacientes = res.body.map((x,i)=>{
                    paciente = {
                        cpf: x.cpf,
                        nome: x.nome,
                        endereco: x.endereco,
                        dataNascimento: new Date(x.dataNascimento).toLocaleDateString("en-US")
                    }   
                    return paciente;
                });                
            })
            .catch(er => {
                console.log('GET_Pacientes: error'+er);
            });  
    return pacientes;
}

export const getMedicos = async (cpf) => {
    var medicos = new Array();
    var rota;
    if(cpf.length == 0)
        rota = IP_ADDRES + 'queries/getMedicos';
    else
        rota = IP_ADDRES + `queries/getMedico?cpf=${cpf}`;
    console.log('GET_Medico: '+rota);
    
    await request.get(rota)
            .set('x-access-token', 'yl8CI1ON7mUDgew5DoiFOCSK8dwYGFU3ENNSvZ4VdXFfzbI2EaLhW1oafyB4sqCq')
            .then(res => {
                console.log('GET_Medico: '+JSON.stringify(res.body));
                console.log('GET_Medico: '+res.status);      
                medicos = res.body;
            })
            .catch(er => {
                console.log('GET_Medico: error'+er);
            });  
    return medicos;
}

export const getConsultas = async (numCpf) => {
    var consultas = new Array();
    var consulta;
    var rota;
    rota = IP_ADDRES + `queries/getConsultasProntuario?numProntuario=resource%3Aorg.pep.blockchain.paciente.Paciente%23${numCpf}`;
    console.log('GET_Consultas: '+rota);
    
    await request.get(rota)
            .set('x-access-token', 'yl8CI1ON7mUDgew5DoiFOCSK8dwYGFU3ENNSvZ4VdXFfzbI2EaLhW1oafyB4sqCq')
            .then(res => {
                console.log('GET_Consultas: '+JSON.stringify(res.body));
                console.log('GET_Consultas: '+res.status);       
                consultas = res.body.map((x,i)=>{
                    consulta = {
                        medico : x.medico,
                        tipo : x.tipo,
                        data : x.data,
                        anamnese : x.anamnese,
                        historico : x.historico,
                        exameFisico : x.exameFisico,
                        examesSolicitados : x.examesSolicitados,
                        hipotese : x.hipotese,
                        conduta : x.conduta,
                        exames : x.exames.map((x,i)=> x.nomeExame)
                    };
                    return consulta; 
                });
            })
            .catch(er => {
                console.log('GET_Consultas: error'+er);
            });  
    return consultas;

}

export const getHistorian = async () => {
    var historian = new Array();
    var hist;
    var rota = IP_ADDRES + 'system/historian';
    console.log('GET_Historian: '+rota);
    
    await request.get(rota)
            .set('x-access-token', 'yl8CI1ON7mUDgew5DoiFOCSK8dwYGFU3ENNSvZ4VdXFfzbI2EaLhW1oafyB4sqCq')
            .then(res => {
                console.log('GET_Historian: '+JSON.stringify(res.body));
                console.log('GET_Historian: '+res.status);       
                historian = res.body.map((x,i)=>{
                    hist = {
                        id : x.transactionId,
                        tipo : x.transactionType.split('.')[3],
                        paticipante : x.participantInvoking,
                        data : x.transactionTimestamp
                    };
                    if(typeof hist.paticipante != 'undefined')
                        hist.paticipante = hist.paticipante.split('.')[4];
                    return hist; 
                });
            })
            .catch(er => {
                console.log('GET_Historian: error'+er);
            });  
    historian.sort((x,y)=>{
        return new Date(y.data) - new Date(x.data);
    });
    return historian;
}

export const getTransacao = async (tipo, id) => {
    var transacao;
    var rota = IP_ADDRES + tipo+ '/'+id;
    console.log('GET_Transacao: '+rota);
    
    await request.get(rota)
            .set('x-access-token', 'yl8CI1ON7mUDgew5DoiFOCSK8dwYGFU3ENNSvZ4VdXFfzbI2EaLhW1oafyB4sqCq')
            .then(res => {
                console.log('GET_Transacao: '+JSON.stringify(res.body));
                console.log('GET_Transacao: '+res.status);       
                transacao = res.body;
            })
            .catch(er => {
                console.log('GET_Transacao: error'+er);
            });  
            
    return transacao;
}